
public class Validate {
    final static int NB_BYTES_PER_IP_ADRESS = 4;

    // 123.233.222 ===> [[123], [233], [222]]
    static boolean validateIp(String ipAdress){

        String[] splittedIntoBytes = ipAdress.split("\\.");

        if(splittedIntoBytes.length != NB_BYTES_PER_IP_ADRESS){
            System.out.println("Invalid IP adress, please try again.");
            return false;
        }
        for(String aByte : splittedIntoBytes){


            try{
                int thisByte = Integer.parseInt(aByte);
                if(thisByte > 255 || thisByte < 0){
                    System.out.println("Invalid IP adress, please try again.");
                    return false;
                }
            }
            catch(NumberFormatException e) {
                System.out.println("Invalid IP adress, please try again.");
                return false;
            }

        }
        return true;
    }

    //returns true if value is between 5000 and 5050
    static boolean validatePort(int ipAdress){
        if(ipAdress >= 5000 && ipAdress <= 5050)
            return true;
        System.out.println("Invalid port number, please try again.");
        return false;
    }
}
