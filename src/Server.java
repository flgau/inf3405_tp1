import java.net.*;
import java.io.*;

public class Server {
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;
    private String ipAdress;
    private int port;
    private static ServerSocket listener;//

    public Server() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        do{
            System.out.println("Please enter IP Adress : ");
            ipAdress = reader.readLine();
        }while(!Validate.validateIp(ipAdress));

        do{
            System.out.println("Please enter port number : ");
            port = Integer.parseInt(reader.readLine());
        }while(!Validate.validatePort(port));


        try{
            listener = new ServerSocket(port);
            System.out.println("Server is listening");
            System.out.println("Waiting for connection ...");
            socket = listener.accept();
            System.out.println("Client accepted");
            input = new DataInputStream(
                    new BufferedInputStream(socket.getInputStream()));
            String line = "";
            // reads message from client until "Over" is sent
            while (!line.equals("Over"))
            {
                try
                {
                    line = input.readUTF();
                    System.out.println(line);

                }
                catch(IOException i)
                {
                    System.out.println(i);
                }
            }
            System.out.println("Closing connection");

            // close connection
            socket.close();
            input.close();
        }

        catch(IOException i) {
            System.out.println(i);
        }

    }

    public static void main(String[] args) throws IOException{
        Server server = new Server();

    }



}
