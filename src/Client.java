import java.net.*;
import java.io.*;

public class Client {
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;
    private String ipAdress;
    private int port;

    public Client() throws IOException{ //changer en main?


        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        do{
            System.out.println("Please enter IP Adress : ");
            ipAdress = reader.readLine();
        }while(!Validate.validateIp(ipAdress));

        do{
            System.out.println("Please enter port number : ");
            port = Integer.parseInt(reader.readLine());
        }while(!Validate.validatePort(port));


        try{
            socket = new Socket(ipAdress, port);
            System.out.println("Connection is established.");
            input = new DataInputStream(System.in);
            out = new DataOutputStream(socket.getOutputStream());
        }
        catch(UnknownHostException e){
            System.out.println(e);

        }
        catch(IOException e) {
            System.out.println(e);
        }
        //CC -----------
        String line = "";

        // keep reading until "Over" is input
        while (!line.equals("Over"))
        {
            try
            {
                line = input.readLine();
                out.writeUTF(line);
            }
            catch(IOException i)
            {
                System.out.println(i);
            }
        }
        try
        {
            input.close();
            out.close();
            socket.close();
        }
        catch(IOException i)
        {
            System.out.println(i);
        }
        //------------


    }
    public static void main(String[] args) throws IOException{
        Client client = new Client();
    }
}
